[![Build Status](https://travis-ci.org/thibmaek/modular-snippets.svg)](https://travis-ci.org/thibmaek/modular-snippets)

# modular-snippets
Since Atom core has no way of dividing snippets in partials or modules this is a hacky solution to use a more modular and shorter snippets file.

## Getting started
This package is a boilerplate package to append upon.  
Install it via the APM frontend or with `apm install modular-snippets` to clone it to your .atom/packages folder.

Locate the cloned repo (possibly change the title if you want too) and for every language you need, just create a new file in `snippets/`. Two example files have already been given.

This is really the most barebones starting point there is to creating a modular snippets file. Hopefully Atom's dev team introduces some kind of way to create separate snippetsheets in future Atom versions and this package becomes obsolete.
